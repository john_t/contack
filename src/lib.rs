#![cfg_attr(feature = "dox", feature(doc_cfg))]
#![doc = include_str!("../README.md")]

#![warn(clippy::all, clippy::pedantic, clippy::cargo, missing_docs)]
#![allow(
    clippy::doc_markdown,
    clippy::module_name_repetitions,
    clippy::wildcard_imports,
    clippy::missing_const_for_fn,
    clippy::wrong_self_convention,
    clippy::redundant_feature_names
)]

mod address;
pub mod contact_information;
pub mod contact_platform;
pub mod date_time;
mod name;
mod org;
mod uri;
mod gender;

#[cfg(feature = "lazy_static")]
#[macro_use]
extern crate lazy_static;
#[cfg(feature = "thiserror")]
#[macro_use]
extern crate thiserror;

#[cfg_attr(feature = "diesel-derive-enum", macro_use)]
#[cfg(feature = "diesel-derive-enum")]
extern crate diesel_derive_enum;

#[cfg(feature = "diesel")]
#[macro_use]
extern crate diesel;

#[cfg(feature = "diesel_migrations")]
#[macro_use]
extern crate diesel_migrations;

#[cfg(feature = "diesel_support")]
#[cfg_attr(feature = "dox", doc(cfg(feature = "diesel_support")))]
pub mod schema;

#[cfg(feature = "sql")]
#[cfg_attr(feature = "dox", doc(cfg(feature = "sql")))]
pub mod sql;

#[cfg(feature = "sql")]
pub(crate) use sql::SqlConversionError;

#[cfg(feature = "read_write")]
#[cfg_attr(feature = "dox", doc(cfg(feature = "read_write")))]
pub mod read_write;

#[cfg(feature = "read_write")]
use crate::read_write::component::Component;
#[cfg(feature = "read_write")]
use crate::read_write::error::FromComponentError;

pub use {
    address::{Address, Geo},
    contact_information::{ContactInformation, Type},
    contact_platform::ContactPlatform,
    date_time::DateTime,
    name::Name,
    org::Org,
    uri::Uri,
    gender::{Gender, Sex}
};

mod contact;
pub use contact::Contact;
