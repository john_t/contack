//! Represents a platform of which a `ContactInformation` is on

/// Represents a platform of which a `ContactInformation` is on.
///
/// For example an email will use `Email` and a phone number `Tel`.
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd)]
#[cfg_attr(
    feature = "diesel-derive-enum",
    derive(DbEnum),
    DieselType = "Contack_contact_platform",
    db_rename = "Contack_contact_platform"
)]
#[cfg_attr(
    feature = "sqlx",
    derive(sqlx::Type),
    sqlx(type_name = "contack_contact_platform"),
    sqlx(rename_all = "PascalCase")
)]
pub enum ContactPlatform {
    /// An email
    Email,
    /// A phone number
    Tel,
    /// A discord
    Discord,
    /// A matrix
    Matrix,
    /// A skype username/email
    Skype,
    /// For AOL instant messanger
    Aim,
    /// Jabber
    Jabber,
    /// Icq
    Icq,
    /// Groupwise
    Groupwise,
    /// Gadugadu
    Gadugadu,
    /// Xmpp property in VCard. You should specify a
    /// Uri in the `ContactInformation`
    Unknown,
}

impl From<u8> for ContactPlatform {
    fn from(int: u8) -> Self {
        match int {
            0 => Self::Email,
            1 => Self::Tel,
            2 => Self::Discord,
            3 => Self::Matrix,
            4 => Self::Skype,
            5 => Self::Aim,
            6 => Self::Jabber,
            7 => Self::Icq,
            8 => Self::Groupwise,
            9 => Self::Gadugadu,
            _ => Self::Unknown,
        }
    }
}

impl From<ContactPlatform> for u8 {
    fn from(cp: ContactPlatform) -> Self {
        match cp {
            ContactPlatform::Email => 0,
            ContactPlatform::Tel => 1,
            ContactPlatform::Discord => 2,
            ContactPlatform::Matrix => 3,
            ContactPlatform::Skype => 4,
            ContactPlatform::Aim => 5,
            ContactPlatform::Jabber => 6,
            ContactPlatform::Icq => 7,
            ContactPlatform::Groupwise => 8,
            ContactPlatform::Gadugadu => 9,
            ContactPlatform::Unknown => 10,
        }
    }
}

impl std::fmt::Display for ContactPlatform {
    // Formats this
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Email => "Email",
                Self::Tel => "Phone",
                Self::Discord => "Discord",
                Self::Matrix => "Matrix",
                Self::Skype => "Skype",
                Self::Aim => "Aim",
                Self::Jabber => "Jabber",
                Self::Icq => "ICQ",
                Self::Groupwise => "GroupWise",
                Self::Gadugadu => "GaduGadu",
                Self::Unknown => "Unknown",
            }
        )
    }
}

impl Default for ContactPlatform {
    fn default() -> Self {
        Self::Unknown
    }
}
