table! {
    use diesel::sql_types::*;
    contacts (uid) {
        uid -> Text,
        name_given -> Nullable<Text>,
        name_additional -> Nullable<Text>,
        name_family -> Nullable<Text>,
        name_prefixes -> Nullable<Text>,
        name_suffixes -> Nullable<Text>,
        nickname -> Nullable<Text>,
        anniversary -> Nullable<Timestamp>,
        bday -> Nullable<Timestamp>,
        gender_gender -> Nullable<Text>,
        gender_sex -> Nullable<Text>,
        photo_uri -> Nullable<Text>,
        photo_bin -> Nullable<Bytea>,
        photo_mime -> Nullable<Text>,
        title -> Nullable<Text>,
        role -> Nullable<Text>,
        org_org -> Nullable<Text>,
        org_unit -> Nullable<Text>,
        org_office -> Nullable<Text>,
        logo_uri -> Nullable<Text>,
        logo_bin -> Nullable<Bytea>,
        logo_mime -> Nullable<Text>,
        home_address_street -> Nullable<Text>,
        home_address_locality -> Nullable<Text>,
        home_address_region -> Nullable<Text>,
        home_address_code -> Nullable<Text>,
        home_address_country -> Nullable<Text>,
        home_address_geo_longitude -> Nullable<Double>,
        home_address_geo_latitude -> Nullable<Double>,
        work_address_street -> Nullable<Text>,
        work_address_locality -> Nullable<Text>,
        work_address_region -> Nullable<Text>,
        work_address_code -> Nullable<Text>,
        work_address_country -> Nullable<Text>,
        work_address_geo_longitude -> Nullable<Double>,
        work_address_geo_latitude -> Nullable<Double>,
    }
}

table! {
    use crate::contact_platform::Contack_contact_platform;
    use crate::contact_information::Contack_contact_information_type;
    use diesel::sql_types::*;
    contacts_contact_information (pid) {
        pid -> Text,
        pref -> SmallInt,
        value -> Text,
        platform -> Contack_contact_platform,
        typ -> Nullable<Contack_contact_information_type>,
        uid -> Text,
    }
}

allow_tables_to_appear_in_same_query!(contacts, contacts_contact_information,);
