#[cfg(feature = "read_write")]
use crate::read_write::component::Component;

/// Represents an organisation.
#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Org {
    /// The organisation, ie "ABC, Inc."
    pub org: String,
    /// The unit, ie "North American Division"
    pub unit: String,
    /// The offiuce, ie "Marketing"
    pub office: String,
}

impl Org {
    /// Creates a new [`Org`]
    #[must_use]
    pub const fn new(org: String, unit: String, office: String) -> Self {
        Self { org, unit, office }
    }
}

#[cfg(feature = "read_write")]
impl From<Org> for Component {
    fn from(org: Org) -> Self {
        Self {
            name: "ORG".to_string(),
            values: vec![vec![org.org], vec![org.unit], vec![org.office]],
            ..Self::default()
        }
    }
}

#[cfg(feature = "read_write")]
impl From<Component> for Org {
    fn from(mut comp: Component) -> Self {
        Self {
            org: comp
                .values
                .get_mut(0)
                .and_then(Vec::pop)
                .unwrap_or_default(),
            unit: comp
                .values
                .get_mut(1)
                .and_then(Vec::pop)
                .unwrap_or_default(),
            office: comp
                .values
                .get_mut(2)
                .and_then(Vec::pop)
                .unwrap_or_default(),
        }
    }
}
