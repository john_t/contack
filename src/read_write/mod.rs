//! # Read Write
//!
//! This module contains items required for serialisation
//! and deserialisation.

pub mod component;
pub use component::Component;
pub mod error;
pub mod escape;
pub mod vcard;
pub use vcard::VCard;
